import 'package:ecoins/components/FooteExample.dart';
import 'package:ecoins/components/HeaderExample.dart';
import 'package:ecoins/screens/home_example/HomeBodyExample.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeExample extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            HeaderExample(),
            HomeBodyExample(),
            FooterExample(),
          ],
        ),
      ),
    );
  }
}
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Example {
  String attribute1;
  String attribute2;
  String attribute3;

  Example(this.attribute1, this.attribute2, this.attribute3);

  Map<String, dynamic> toJson() =>
      {
        'attribute1': attribute1,
        'attribute2': attribute2,
        'attribute3': attribute3,
      };

  Example.fromJson(Map<String, dynamic> json)
      :this.attribute1 = json["attribute1"],
        this.attribute2 = json["attribute2"],
        this.attribute3 = json["attribute3"];
}

class SharedPreferences_Example{
  //Cach luu object vao SharedPreferences
  void StorageObjectToSharedPreferences() async{
    Example e = new Example("Attribute 1", "Attribute 1", "Attribute 1");
    final SharedPreferences prefer = await SharedPreferences.getInstance();
    prefer.setString("ExampleToken", json.encode(e.toJson()));
  }

  //Cach lay object tu SharedPreferences roi parse sang object hoac map
  void GetObjectFromSharedPreferences() async{
    final SharedPreferences prefer = await SharedPreferences.getInstance();
    Example e = Example.fromJson(json.decode(prefer.get("ExampleToken")));

    Map map = json.decode(prefer.get("ExampleToken"));
  }
}
